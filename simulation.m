%   AMNTL
close all;clc; clearvars;

nF = 51; %min 21
Image.F = linspace(26.5,40,nF); %GHz
c = 299.795638; %speed of light
K = 2*pi*Image.F/c;

%%  Variables
%% Define antenna array
dX = 1; %mm 
dY = 1;
maxX = 50; %mm
minX = -maxX;   
Image.x = minX:dX:maxX;
Image.y = minX:dX:maxX; % of array


%% Define depth
dZ = 0.25;

target(1) = 25;
td = 50;
for ii=1:numel(target)
    sigma(ii) = 1;
    tloc(ii,:) = [0 0 sum(target(1:ii))];
end

dielectric = [1];

SA = minX:dX/4:maxX; % synthetic aperture
% sigma = [1 0 0 0 0]; %for each layer
% tloc = [0 0 d(1)]; % [X Y Z] location
% t2loc = [0 0 sum(d(1:2))]; % [X Y Z] location
% plateloc = [SA; zeros(size(SA)); repelem(sum(d(1:3)),size(SA))];
% mloc = [0 0 sum(d(1:4))]; % [X Y Z] location
% m2loc = [0 0 sum(d(1:4))]; % [X Y Z] location

% Alpha = tan(2*Standoff/maxX);
% Beta = tan(2*(Standoff+Separation)/maxX);


%%  Define Environments
% Pre-Allocate
SigT = zeros(numel(target),numel(Image.x),numel(Image.y),numel(Image.F));

% Radius from array points to target
for ii=1:numel(target)
Rad(ii,:) = sqrt((Image.x-tloc(ii,1)).^2+(Image.y-tloc(ii,2)).^2+(tloc(ii,3))^2);
end

for kindex = 1:numel(K)
    for R = 1:numel(Rad)
        for ii=1:numel(target)
        % accounts for distance
        SigT(R,:,kindex,ii) = sigma(ii) *exp(-2j*K(kindex)*Rad(ii,R)) / Rad(ii,R).^2;
        end
    end
end
Image.RawData = sum(SigT,4); % sum all targets


    figure; imagesc(abs(squeeze(Image.RawData(:,:,target(1)))));
    title(sprintf("Raw Data of Target %i",1));

%%  SAR
Image.z = 0:td;
for depth = 1:length(Image.z)
    Settings.SARLayerThickness = Image.z(depth);
    Settings.SARLayerDielectric = 1;
    temp = SAR(Image,dX,dY,Settings);
    Image.I(:,:,depth) = temp.Data; 
end


volumeViewer(abs(Image.I));
%%  Plotting
% figure; axis image;
% imagesc(X,-1:1,abs(I(:,:,Standoff).'))
% title('X by Y of Target')
% xlabel('X (Array)'); ylabel('Y');
[~, imin] = min(abs(Image.x));
zsig = squeeze(abs(Image.I(imin,1,:)));
% v = linspace(min(Z),max(Z),numel(Z)*4)
% Z = interp1(v,Zi,'spline')
% zsig = interp1(v,zsi,'spline')

% [pks, locs, w, p] = findpeaks(zsig, 'MinPeakProminence',3);

maxi = max(zsig);
hp = .7071*maxi;
above = zsig.*(zsig>=hp);
above(above<hp) = NaN;
numabove = (numel(above) - sum(isnan(above)))/4;
[pks,locs,w,p] = findpeaks(zsig,1);
% [pks,locs,w,p] = findpeaks(zsig,'MinPeakHeight',0.7071*max(zsig));
maximums = pks;

[~, y1] = min(abs(zsig(1:locs(1))-hp));
[~, y2] = min(abs(zsig(locs(1):end)-hp));

% BW = Z(y2+locs(1)-1) - Z(y1)
% Angle = (Beta-Alpha);

figure
subplot 211
hold on;
set(gca,'YDir','reverse')
imagesc(Image.x,Image.z,abs(squeeze(Image.I(:,1,:)).'))
line([minX,maxX], [d(1),d(1)], 'Color', 'g','LineStyle','--','linewidth',1);
line([minX,maxX], [sum(d(1:2)), sum(d(1:2))], 'Color', 'w','LineStyle','--','linewidth',1);
line([minX,maxX], [sum(d(1:3)), sum(d(1:3))], 'Color', 'c','LineStyle','--','linewidth',1);
title('SIMULATED')
colorbar
colormap jet
axis image

subplot 212
grid on; hold on
plot(Image.z,zsig,'LineWidth',2);
plot(Image.z,above,'LineWidth',2);
% plot(Z(y1),zsig(y1),'r*')
% plot(Z(14),zsig(14),'r*') %y2+locs(1)-1
plot(Z(locs+1),zsig(locs+1),'r*')
line([d(1), d(1)],ylim,  'Color', 'r','LineStyle','--','linewidth',1);
line([sum(d(1:2)), sum(d(1:2))],ylim,  'Color', 'k','LineStyle','--','linewidth',1);
line([sum(d(1:3)), sum(d(1:3))],ylim,  'Color', 'b','LineStyle','--','linewidth',1);
xlabel('Depth (mm)')
ylabel('Absolute Image Value')
title(sprintf('%i mm standoff; %i mm array',d(1),maxX*2))