% AMNTL
% A class possessing basic method traits like a boolean to control Verbose
% output and the number of threads to utilize (nThreads)
%
% If nThreads is set to 0, the method will auto-detect the maximum number
% of threads and use that

classdef sailMethod < handle
   properties
       nThreads % Number of threads for SAIL (mex) methods to use
       Verbose  % If true, report intermediate information to the screen
   end
   
   methods
       function O = sailMethod(I)
           if (nargin == 0) || isempty(I)
               I.nThreads = 0;
               I.Verbose = false;
           end
           
           % Copy method properties from one object to another
           O.nThreads = I.nThreads;
           O.Verbose = I.Verbose;
       end
       
       function set.nThreads(O,In) % nThreads must be non-zero integer
           if (In >= 0)
               O.nThreads = int32(In);
           else
               error('Could not process input for nThreads');
           end
       end
       
       function set.Verbose(O,In) % nThreads must be non-zero integer
           if islogical(In)
               O.Verbose = In;
           else
               error('Could not process input for Verbose');
           end
       end
       
   end
end
