%   AMNTL
close all;
clc; clearvars;
load("RealTargets.mat");
load("GhostTargets.mat");

meas_noise = 40;% SNR;
%% USER DEFINED
Band = 'ka';
nF = 201; %min 21

aD = 2; %1 or 2 dimension aperture
ss = [1 1]; % (mm)
tot_size = [100 100];
%% Set band
Image.F = linspace(26.5,40,nF);
c = 299.795638; %   speed of light in mm.GHz
K = 2*pi*Image.F/c;   %   free space wave number (frequency in GHz)

dielectric = [1];
Settings.K = K;
Settings.SARLayerDielectric = dielectric;
%%  Define Targets
target(1) = 20;
% target(1) = 0;
target(2) = 10;
% plate = 20;
% target(3) = plate;
% target(4) = plate+(plate-target(2));
% sigma = [-1 -1 -1 1];
nT = numel(target);

% sigma = [-1 1];
for ii=1:nT
    sigma(ii) = -1;
    tloc(ii,:) = [0 0 sum(target(1:ii))];
end

%% Define antenna array
if aD ==1 
    Image.x = -tot_size(1)/2:ss(1):tot_size(1)/2;
    Image.y = 0;
elseif aD ==2
    Image.x = -tot_size(1)/2:ss(1):tot_size(1)/2;
    Image.y = -tot_size(2)/2:ss(2):tot_size(2)/2;
else
    error("Synthetic Aperture must be 1 or 2-D");
end

%% Define depth
dZ = 1;
td = 40;
Image.z = 0:dZ:td;

nX = numel(Image.x);
nY = numel(Image.y);
nZ = numel(Image.z);
nF = numel(Image.F);

%%  Define Environments
[Ap.X,Ap.Y] = ndgrid(Image.x,Image.y);
Ap.X = Ap.X(:); Ap.Y = Ap.Y(:);

% [Vol.X,Vol.Y] = ndgrid(Image.x,Image.y,Image.z);
% Vol.X = Vol.X(:); Vol.Y = Vol.Y(:); Vol.Z = Vol.Z(:);

Rad = zeros(nT,nX*nY);% Pre-Allocate
for iTar=1:numel(target)% Calculate radius from array points to target
Rad(iTar,:) = sqrt((Ap.X-tloc(iTar,1)).^2+(Ap.Y-tloc(iTar,2)).^2+(tloc(iTar,3))^2);
end
% for ii=1:numel(target)
% Rad(ii,:) = sqrt((Image.x-tloc(ii,1)).^2+(Image.y-tloc(ii,2)).^2+(tloc(ii,3))^2);
% end
Image.TDist = reshape(Rad,[nT,nX,nY]);
% imagesc(Image.x,Image.y,squeeze(Image.TDist).');

SigT = zeros(nT,nX*nY,nF);% Pre-Allocate
for iK = 1:numel(K)
    for iXY = 1:size(Rad,2)
        for iTar=1:nT
        % accounts for distance
        SigT(iTar,iXY,iK) = sigma(iTar).*exp(-2j*K(iK).*Rad(iTar,iXY))...
                            ./Rad(iTar,iXY).^2;
        end
    end
end
Image.RawData = squeeze(sum(SigT,1));% sum all targets
Image.RawData = reshape(Image.RawData,[nX,nY,nF]);
sig = awgn(Image.RawData,meas_noise);
% volumeViewer(abs(Image.RawData));
% figure; 
% % orthosliceViewer(Image.RawData);
% imagesc(Image.F,Image.x,squeeze(abs(Image.RawData(:,1,:))).');
% xlabel("Frequency");ylabel("X");
% title("Raw Data");

%%  SAR
for depth = 1:nZ
    Settings.SARLayerThickness = Image.z(depth);
    temp = SAR(Image,Settings);
    Image.SARdata(:,:,depth) = temp.Data; 
end

%%  Plotting
minX = min(Image.x);
maxX = max(Image.x);
% volumeViewer(abs(Image.SARdata));

% for ii = [1 5 10 20]
%     figure; 
%     imagesc(Image.y,Image.x,abs(squeeze(Image.SARdata(:,:,ii))).');
%     title(sprintf("SAR Data of Target %i",ii));
%     xlabel('X (mm)'); ylabel('Y (mm)');
% end

%% FINAL PLOT
XZslice = abs(squeeze(Image.SARdata(:,ceil(end/2),:)));
% figure
% % subplot 211; 
% hold on;
% set(gca,'YDir','reverse')
% imagesc(Image.x,Image.z,XZslice.');
% % line([minX,maxX], [target(1),target(1)], 'Color', 'g','LineStyle','--','linewidth',1);
% title('SAR Data (Side View)');
% xlabel("X (mm)"); ylabel("Depth (mm)");
% colorbar; colormap jet; axis image

[~, xloc] = min(abs(Image.x-tloc(1)));
[~, yloc] = min(abs(Image.y-tloc(2)));
sig = squeeze(Image.SARdata(xloc,yloc,:));
zsig = squeeze(abs(sig));


% figure; hold on;
% scatter(Image.z(2:end),abs(cRT(:,1,2)),50,'filled');
% scatter(Image.z(2:end),abs(cGT(:,1,2)),50,'filled');

maxi = max(zsig);
hp = .7071*maxi;
above = zsig.*(zsig>=hp);
above(above<hp) = NaN;
numabove = (numel(above) - sum(isnan(above)))/4;
[pks,locs,w,p] = findpeaks(zsig,1);
% [pks,locs,w,p] = findpeaks(zsig,'MinPeakHeight',0.7071*max(zsig));
maximums = pks;

[~, y1] = min(abs(zsig(1:locs(1))-hp));
[~, y2] = min(abs(zsig(locs(1):end)-hp));

comb1 = RT(:,20)+RT(:,30);
comb2 = RT(:,20)+GT(:,30);

cRT = corrcoef(sig,comb1);
cGT = corrcoef(sig,comb2);

figure;
% subplot 212
grid on; hold on
plot(Image.z,zsig,'b','LineWidth',2);

abo = above;
plot(Image.z,abs(comb1),'g--','LineWidth',4);
plot(Image.z,abs(comb2),'k--','LineWidth',4);
plot(Image.z(locs+1),zsig(locs+1),'r*')
% line([target(1), target(1)],ylim,  'Color', 'r','LineStyle','--','linewidth',1);
xlabel('Depth (mm)')
ylabel('Absolute Image Value')
legend("Measured Signal","Real Targets","Real & Ghost Target");
% load("tandg.mat");
% plot(Image.z,above,'c-','LineWidth',2);
% plot(Image.z,abo,'r-','LineWidth',2);
% legend("Both Real","Real & Ghost");


% Fsig = abs(fftshift(fft(sig)));
% FGT = abs(fftshift(fft(GT(:,30))));
% NS = Fsig./FGT;
% ns = ifft(ifftshift(NS));
% [q,r] = deconv(sig,GT(:,30))
ydc=ifft(fft(sig)./fft(GT(:,30))).*sum(GT(:,30));
SmoothWidth=8; % Adjust for best results to reduce noise.
sy=fastsmooth(ydc,SmoothWidth,3);% Take just the first N elements
figure; plot(abs(sy));

Deconvoluted=SegGaussDeconvPlot(sig,GT(:,30),3)







