function I  = SAR(Image,Settings)
%%% AMNTL
%%% This function runs a SAR FFT algorithm on data
%%% Settings contains SAR settings
%%% Image is the struct containing raw data
%%% I is the struct containing the processed data
%%%%%%%%%%%


% %   Stores Step Size
dx = abs(Image.x(2)- Image.x(1));
dy = abs(Image.y(2)- Image.y(1));

%   Creates spatial frequency vectors
kx = ([0:(length(Image.x)-1)] - ceil((length(Image.x)-1)/2)) / (length(Image.x)*dx/(2*pi));
ky = ([0:(length(Image.y)-1)] - ceil((length(Image.y)-1)/2)) / (length(Image.y)*dy/(2*pi));
% convert spatial frequency vectors in matrices
[Kx,Ky] = ndgrid(kx,ky);

%   Performs a fast Fourier transform on two-dimentional (2D) data for each frequency using Matlab's fft2() function
%   recenters the 2D data to zero-frequency using Matlabs's fftshift()
%   calculates the 2D SAR data using the standoff distance, dielectric
%   constant, frequency-dependent wave number, and spatial freqyency
%   vectors
%   shifts the 2D data back to the original portion of the spectrum using
%   Matlab's ifftshift() function
%   Performs an inverse fast Fourier transform on 2D data using Matlab's 
%   ifft2() function
for index_f = 1:numel(Image.F)
    S = fft2(Image.RawData(:,:,index_f));
    S(1,1) = 0;                
    S = fftshift(S);
    S = S.* exp(1j*Settings.SARLayerThickness*sqrt(Settings.SARLayerDielectric)...
        *sqrt(4*Settings.K(index_f).^2 - Kx.^2 - Ky.^2));
    Int(:,:,index_f) = ifft2(ifftshift(S));
end
%   Sums the data across the frequency domain to generate a 2D SAR image
%   focused on the target (surface of the steel sheet)
Int = sum(Int,3);

% if ~Settings.KeepPadding %  Remove zero pad data points
%     %   number of data points in each dimension without padding
%     newX = numel(Image.X) /(1 + Settings.ZeroPadX);
%     newY = numel(Image.Y) /(1 + Settings.ZeroPadY);
%     
%     %   number of data points to be removed from each end in each dimension
%     SX = round((numel(Image.X)-newX)/(dx));
%     SY = round((numel(Image.Y)-newY)/(dy));
%     %   Creates new X & Y vectors    
%     if SX == 0
%         I.X = Image.X;
%         SX = SX + 1;
%     else
%         I.X = Image.X(SX+1:end-SX+1);
%     end
%     if SY == 0
%         I.Y = Image.Y;
%         SY = SY +1;
%     else
%         I.Y = Image.Y(SY+1:end-SY+1);
%     end
%     %   Crops the zero padding out of the data field
%     I.Data = Int(SX:end-SX+1,SY:end-SY+1);
% else %  Vector sizes will not change
%     I.X = Image.x;
%     I.Y = Image.y;
    I.Data = Int;
% end

end

