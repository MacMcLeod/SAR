%   AMNTL
close all;  clc; clearvars;
c = 299.795638; % speed of light
scans =["plasticonfoam.mat"];

%% Load Scan
load(scans)
F = linspace(26.5,40,numel(Data(1,1,:)));
K = 2*pi*F/c;
% im = flip(squeeze(Data(:,round(end/2),:)),2);
% [tmp ind] = max(Data(:,end/2,22))
use = mean(Data(:,18:23,:),2);
im = squeeze(use);
ZSIGNAL = abs(sum(im(round(end/2),:),1));
ZSIGNAL = ZSIGNAL/max(ZSIGNAL);
hp = .7071*max(ZSIGNAL);
above = ZSIGNAL.*(ZSIGNAL>=hp);
above(above<hp) = NaN;


scans =["plasticoncb.mat"];
load(scans)
use = mean(Data(:,18:23,:),2);
im = squeeze(use);
ZSIGNAL2 = abs(sum(im(round(end/2),:),1));
ZSIGNAL2 = ZSIGNAL2/max(ZSIGNAL2);
hp = .7071*max(ZSIGNAL2);
above2 = ZSIGNAL2.*(ZSIGNAL2>=hp);
above2(above2<hp) = NaN;
%% Plotting
figure;
hold on;
imagesc(X,Z,abs(squeeze(im).'));%
axis image
title(sprintf('%s',scans))
xlabel('X'); ylabel('Depth') 
colormap jet;

figure
hold on
plot(Z,ZSIGNAL,'linewidth',2)
% plot(Z,above,'LineWidth',2);
plot(Z+2,ZSIGNAL2,'linewidth',2)
% plot(Z+2,above2,'LineWidth',2);
legend('No Conductor','With Conductor')
set(gca, 'XDir','reverse')
grid on;
xlabel('Depth'); ylabel('Signal Strength')
