%   AMNTL
close all;  clc; clearvars;

c = 299.795638; % speed of light
scans =["2layer10mmsep-bbs-on-foam-25mmS.mat"];

Standoff = -25;
ndBB = [0];
Plate = [0];

%% Load Scan
load(scans)
F = linspace(26.5,40,numel(Data(1,1,:)));
K = 2*pi*F/c;
Settings.SARLayerDielectric = 1;

for Zee = 1:100
    Settings.SARLayerThickness = Zee;
    IMAGE(:,:,Zee) = SAR(Data,X,Y,K,Settings);
end

figure;
imagesc(abs(IMAGE(:,:,25)))

%average across one dimension
%round(end/2)-1:round(end/2)+1
im = squeeze(IMAGE(:,round(end/2),:)); clear IMAGE;

% Da = squeeze(sum(Data(:,35:40,:),2));
% Da = squeeze(Data(:,37,:));
    
% Get 1D zsignal plot
% ZSIGNAL = abs(squeeze(Da(round(end/2),:)));
ZSIGNAL = abs(sum(im,1));
maxi = max(ZSIGNAL);
hp = .7071*maxi;
above = ZSIGNAL.*(ZSIGNAL>=hp);
above(above<hp) = NaN;

% flipdim(Da ,2); 
%% Plotting
Z = linspace(0,Zee,numel(ZSIGNAL));

figure;
subplot 211
hold on;
imagesc(X,Z,abs(squeeze(im).'));%
line([min(X) max(X)], [-Standoff -Standoff], 'Color', 'k','LineStyle','--','linewidth',2);
line([min(X) max(X)], [-Standoff+ndBB, -Standoff+ndBB], 'Color', 'c','LineStyle','--','linewidth',2);
line([min(X) max(X)], [-Standoff+Plate, -Standoff+Plate], 'Color', 'w','LineStyle','--','linewidth',2);
axis image
% view(360,90)
set(gca,'Ydir','Reverse')
title(sprintf('%s',scans))
xlabel('X'); ylabel('Depth')
%     colorbar; 
colormap jet;

subplot 212
Z = linspace(0,Zee,numel(ZSIGNAL));
hold on
plot(Z,ZSIGNAL.','linewidth',2)
plot(Z,above.','LineWidth',2);
line([-Standoff, -Standoff],ylim, 'Color', 'r','LineStyle','--','linewidth',2);
line([-Standoff+ndBB, -Standoff+ndBB],ylim, 'Color', 'm','LineStyle','--','linewidth',2);
line([-Standoff+Plate, -Standoff+Plate],ylim, 'Color', 'b','LineStyle','--','linewidth',2);
title(sprintf('%s',scans))
grid on;
xlabel('Depth'); ylabel('Signal Strength')
